package com.example.teste01.controller;

import com.example.teste01.model.HelloWorldModel;
import com.example.teste01.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/home")
public class HelloWorldController {

    @Autowired
    HelloWorldService worldService;

    @GetMapping("/hello")
    public String helloWorld(){
        return new String("5 * 10 é: " + 5*10 + " :)");
    }
    @GetMapping
    public boolean teste(){
        return false;
    }
    @PostMapping
    public String multiplicar(@RequestBody HelloWorldModel model){
        Integer resultado = worldService.multiplicarDoisValores(model.getVal1(), model.getVal2());
        return "O resultado da multiplicação é: " .concat(String.valueOf(resultado));
    }



}
