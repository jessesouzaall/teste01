package com.example.teste01.service;

import org.springframework.stereotype.Service;

@Service
public class HelloWorldService {
    public Integer multiplicarDoisValores(int val1, int val2){
        return val1*val2;
    }

}
